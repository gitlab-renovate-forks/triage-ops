# frozen_string_literal: true

require_relative './base_job'
require_relative '../../triage/reaction'

module Triage
  module PipelineIncidentSla
    class DevEscalationJob < BaseJob
      include Reaction

      DELAY = 4 * 60 * 60 # 4 hours
      PAGERSLACK_MEMBER_ID = 'U014GHS7QV9'

      private

      def execute(event)
        super

        comment_body = <<~MARKDOWN.chomp
          /label ~"#{Labels::MASTER_BROKEN_INCIDENT_ESCALATION_LABELS[:escalated]}"
        MARKDOWN

        add_comment(comment_body, append_source_link: false) if applicable?
      end

      def applicable?
        super && !today_is_weekend?
      end

      def slack_channel
        PAGERSLACK_MEMBER_ID
      end

      def slack_message
        "devoncall #{event.url}"
      end
    end
  end
end
