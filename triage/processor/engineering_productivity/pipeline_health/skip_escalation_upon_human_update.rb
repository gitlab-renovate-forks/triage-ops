# frozen_string_literal: true

require_relative '../../../triage/event'
require_relative '../../../triage/processor'

module Triage
  module PipelineHealth
    class SkipEscalationUponHumanUpdate < Processor
      react_to 'incident.update', 'issue.note'

      def applicable?
        event.from_master_broken_incidents_project? &&
          event.resource_open? &&
          event.by_team_member? &&
          has_applicable_labels?
      end

      def process
        skip_escalation_upon_human_update
      end

      def documentation
        <<~TEXT
          This processor skips pipeline incident escalation process upon human update.
        TEXT
      end

      private

      def skip_escalation_upon_human_update
        add_comment('/label ~"escalation::skipped"', append_source_link: false)
      end

      def has_applicable_labels?
        (event.label_names &
          [Labels::MASTER_BROKEN_INCIDENT_ESCALATION_LABELS[:skipped], Labels::MASTER_BROKEN_INCIDENT_ESCALATION_LABELS[:escalated]]
        ).empty?
      end
    end
  end
end
