# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/related_issue_finder'

module Triage
  class ApplyLabelsFromRelatedIssue < Processor
    react_to 'merge_request.open', 'merge_request.update'

    def applicable?
      event.from_gitlab_org? &&
        labels_from_related_issue.any?
    end

    def process
      apply_labels_from_related_issue
    end

    def documentation
      <<~TEXT
        This processor applies type, section, stage, and group labels to an MR from a detected related issue.
      TEXT
    end

    private

    def related_issue_labels
      return @related_issue_labels if defined?(@related_issue_labels)

      @related_issue_labels =
        Triage::RelatedIssueFinder.new.find_project_issue_in(event.project_id, event.description)&.labels || []
    end

    def related_issue_type_label
      @related_issue_type_label ||= related_issue_labels.grep(/^type::/)&.first
    end

    def related_issue_section_label
      @related_issue_section_label ||= (related_issue_labels & Hierarchy::Section.all_labels).first
    end

    def related_issue_stage_label
      @related_issue_stage_label ||= (related_issue_labels & Hierarchy::Stage.all_labels).first
    end

    def related_issue_group_label
      @related_issue_group_label ||= (related_issue_labels & Hierarchy::Group.all_labels).first
    end

    def related_issue_type_label_valid?
      related_issue_type_label.present? && related_issue_type_label != 'type::ignore'
    end

    def labels_from_related_issue
      return @labels_from_related_issue if defined?(@labels_from_related_issue)

      @labels_from_related_issue = [].tap do |labels_to_set|
        labels_to_set << related_issue_type_label if related_issue_type_label_valid? && !event.type_label_set?
        labels_to_set << related_issue_section_label unless event.section_label_set?
        labels_to_set << related_issue_stage_label unless event.stage_label_set?
        labels_to_set << related_issue_group_label unless event.group_label_set?
      end.compact
    end

    def apply_labels_from_related_issue
      comment = <<~MARKDOWN.chomp
        /label #{labels_from_related_issue.map { |l| %(~"#{l}") }.join(' ')}
      MARKDOWN
      add_comment(comment, append_source_link: false)
    end
  end
end
