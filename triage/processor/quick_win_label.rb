# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/label_implementation_plan'

module Triage
  class QuickWinLabel < Processor
    include Triage::LabelImplementationPlan

    react_to 'issue.open', 'issue.reopen', 'issue.update'

    def applicable?
      return false unless event.from_gitlab_group?

      # Repeat the comment every time the label is added
      quick_win_label_added? && !requirements_met?
    end

    def process
      post_quick_win_label_guide_comment
    end

    def documentation
      <<~TEXT
        This processor reacts to the `quick win` label being added to an issue enforcing the criteria that were
        decided as necessary requirements for the label to be applied.
        The intent is to help ensure that these issues are truly prepared for community contributions
        and can be easily executed by anyone who picks it up.
      TEXT
    end

    private

    def quick_win_label_added?
      event.added_label_names.include?(Labels::QUICK_WIN)
    end

    def requirements_met?
      has_required_weight? && has_valid_implementation_plan?(event.description)
    end

    def has_required_weight?
      return false if event.weight.nil?

      event.weight >= 0 && event.weight <= 3
    end

    def post_quick_win_label_guide_comment
      add_comment(quick_win_guide_comment.strip, append_source_link: true)
    end

    def quick_win_guide_comment
      <<~MARKDOWN.chomp
        @#{event.event_actor_username} thanks for adding the ~"#{Labels::QUICK_WIN}" label!

        However, it appears that this issue does not meet all of the required criteria for the label to be applied
        so it has been automatically removed.

        You can refer to the [criteria for quick win issues documentation](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/community-contributors-workflows/#criteria-for-quick-win-issues) for an up-to-date list of the requirements.

        /unlabel ~"#{Labels::QUICK_WIN}"
        /label ~"#{Labels::AUTOMATION_QUICK_WIN_REMOVED}"
      MARKDOWN
    end
  end
end
