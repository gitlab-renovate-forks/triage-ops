# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'
require_relative '../triage/changed_file_list'

module Triage
  class HandbookMaintainerOpsNotifier < Processor
    HANDBOOK_MAINTAINER_OPS_PATHS = [
      '.gitlab-ci.yml',
      'assets/',
      'config/',
      'layouts/',
      'scripts/',
      '.gitlab/issue_templates/',
      '.gitlab/merge_request_templates/'
    ].freeze

    react_to 'merge_request.update', 'merge_request.open'

    def applicable?
      event.from_handbook? &&
        handbook_maintainer_ops_related_change? &&
        unique_comment.no_previous_comment?
    end

    def process
      add_comment(review_request_comment, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor requests Maintainers to review merge requests when changes are done to pages related to the Handbook theme or pipeline.
      TEXT
    end

    private

    def handbook_maintainer_ops_related_change?
      changed_file_list.any_change?(HANDBOOK_MAINTAINER_OPS_PATHS)
    end

    def changed_file_list
      @changed_file_list ||= Triage::ChangedFileList.new(event.project_id, event.iid)
    end

    def review_request_comment
      comment = <<~MARKDOWN.chomp
      When you are ready for this MR to be reviewed, please assign a relevant maintainer, or tag `@gitlab-com/content-sites/handbook-tools` as it modifies `.gitlab-ci.yml`, `assets/`, `config/`, `layouts`, `scripts/`, `issue_templates/`, or `merge_request_templates/`.
      MARKDOWN

      unique_comment.wrap(comment).strip
    end
  end
end
