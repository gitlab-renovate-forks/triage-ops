# frozen_string_literal: true

require_relative '../../triage/event'
require_relative '../../triage/processor'
require_relative '../../../lib/constants/labels'

module Triage
  class RequestForHelpLifecycle < Processor
    RFH_PROJECT_ID = 64541115

    react_to 'issue.*'

    # We only respond to:
    #  - Events in the RFH project
    #  - On issues started by support team members
    #  - If the issue is not being triaged by a Support SME
    #  - If the event is not coming from bots
    def applicable?
      event.with_project_id?(RFH_PROJECT_ID) &&
        from_support_team_member?(event.resource_author_id) &&
        !has_label?(Labels::TRIAGE_BY_SUPPORT_LABEL) &&
        !known_automation_author?(event.event_actor_id) &&
        !maybe_automation_author?(event.event_actor_username)
    end

    def process
      if event.issue? && event.new_entity?
        apply_label(Labels::NEWLY_OPENED_RFH_TEAM_LABEL)
      elsif event.note?
        if from_support_team_member?(event.event_actor_id)
          apply_label(Labels::LAST_COMMENT_SUPPORT_TEAM_LABEL)
        else
          apply_label(Labels::LAST_COMMENT_DEV_TEAM_LABEL)
        end
      end
    end

    private

    def apply_label(label)
      add_comment(%(/label ~"#{label}"), append_source_link: false) unless has_label?(label)
    end

    def has_label?(label)
      event.label_names.include?(label)
    end

    def from_support_team_member?(user_id)
      Triage.gitlab_com_support_member_ids.include?(user_id)
    end

    def known_automation_author?(user_id)
      ::Triage::Event::AUTOMATION_IDS.include?(user_id)
    end

    def maybe_automation_author?(user_name)
      user_name.match?(::Triage::Event::GITLAB_SERVICE_ACCOUNT_REGEX)
    end
  end
end
