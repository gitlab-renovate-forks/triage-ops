# frozen_string_literal: true

require_relative 'community_processor'

require_relative '../../triage/leading_organization_detector'

module Triage
  class LabelLeadingOrganization < CommunityProcessor
    react_to 'merge_request.open', 'merge_request.update'

    def applicable?
      no_leading_organization_label? &&
        contribution_from_leading_organization? &&
        !contribution_from_contractor?
    end

    def process
      label_leading_organization
    end

    def documentation
      <<~TEXT
        This processor automatically adds the `Leading Organization` label to merge requests
        opened by a Leading Organization.
      TEXT
    end

    private

    def contribution_from_contractor?
      event.label_names.include?(Labels::CONTRACTOR_CONTRIBUTION_LABEL)
    end

    def no_leading_organization_label?
      !event.label_names.include?(Labels::LEADING_ORGANIZATION_LABEL)
    end

    def contribution_from_leading_organization?
      LeadingOrganizationDetector.new.leading_organization?(event.resource_author.username)
    end

    def label_leading_organization
      add_comment(%(/label ~"#{Labels::LEADING_ORGANIZATION_LABEL}"), append_source_link: false)
    end
  end
end
