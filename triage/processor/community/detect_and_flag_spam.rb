# frozen_string_literal: true

require_relative 'community_processor'

module Triage
  class DetectAndFlagSpam < CommunityProcessor
    SPAM_STRINGS = [
      /rebrand.ly/,
      /slot online bonus/,
      /situs[ \w]+slot/
    ].freeze
    SPAM_PATTERNS_REGEX = Regexp.union(SPAM_STRINGS).freeze

    react_to 'merge_request.open', 'merge_request.update'

    def applicable?
      wider_community_contribution_open_resource? &&
        no_spam_label? &&
        payload_contains_spam?
    end

    def process
      label_spam
    end

    def documentation
      <<~TEXT
        This processor attempts to detect, flag (via a ~Spam label) and close spam merge requests.
      TEXT
    end

    private

    def label_spam
      comment = <<~MARKDOWN.chomp
        This merge request has been found to contain content that is inappropriate and violates the
        [GitLab Website Terms of Use](https://handbook.gitlab.com/handbook/legal/policies/website-terms-of-use/).

        Closing and marking appropriately.

        /relabel ~"#{Labels::SPAM_LABEL}"
        /close
      MARKDOWN
      add_comment(comment, append_source_link: true)
    end

    def no_spam_label?
      !event.label_names.include?(Labels::SPAM_LABEL)
    end

    def payload_contains_spam?
      "#{event.title} #{event.description}".match?(SPAM_PATTERNS_REGEX)
    end
  end
end
