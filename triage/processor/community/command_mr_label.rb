# frozen_string_literal: true

require 'digest'

require_relative 'community_processor'

module Triage
  class CommandMrLabel < CommunityProcessor
    include RateLimit

    LABELS_REGEX = /~"([^"]+)"|~([^ ]+)/
    ALLOWED_LABEL_SCOPES = %w[bug feature group maintenance type].freeze
    ALLOWED_LABELS_REGEX = /\A(#{ALLOWED_LABEL_SCOPES.join('|')})::[^:]+\z/
    CATEGORY_LABELS_REGEX = /\A(Category):[^:]+\z/
    EXTRA_ALLOWED_LABELS = [
      'ActivityPub',
      'automation:ml wrong',
      'aws:first-review-approved',
      'aws:second-review-approved',
      'backend',
      'database',
      'documentation',
      'frontend',
      'handbook',
      'pipeline:run-all-jest',
      'pipeline:run-all-rspec',
      'UX',
      'workflow::blocked',
      'workflow::in dev',
      'workflow::ready for review',
      'reproduced on GitLab.com'
    ].freeze
    EXTRA_ALLOWED_LABELS_ADD_ONLY = [
      'security'
    ].freeze
    EXTRA_ALLOWED_LABEL_SCOPES_ADD_ONLY = %w[severity].freeze
    EXTRA_ALLOWED_LABELS_REGEX_ADD_ONLY = /\A(#{EXTRA_ALLOWED_LABEL_SCOPES_ADD_ONLY.join('|')})::[^:]+\z/

    react_to 'issue.note', 'merge_request.note'
    define_command name: %w[label unlabel], args_regex: LABELS_REGEX

    def applicable?
      valid_command?
    end

    def process
      post_label_command
    rescue Gitlab::Error::BadRequest => e
      # Ignore errors when the label doesn't exist. We could also post a note to the author to tell them that the label doesn't exist...
      return if e.response_status == 400 && e.response_message.include?(%(:note=>["can't be blank"]))

      raise e
    end

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("reactive-labeler-commands-sent-#{event.event_actor_id}")
    end

    def documentation
      <<~TEXT
        This processor supports the "@gitlab-bot label" triage operation command to add or remove labels to issues and merge requests.
      TEXT
    end

    private

    def feedback_message
      if labels.empty?
        "No labels specified"
      elsif labels.size != labels_to_apply.size
        invalid_labels_list = invalid_labels.map { |label| %(~"#{label}") }.join(', ')
        "Could not add the following labels: #{invalid_labels_list}"
      end
    end

    def quick_action_command_name
      @quick_action_command_name ||= command.command(event)
    end

    def extra_allowed_label?(label)
      return true if EXTRA_ALLOWED_LABELS.include?(label)

      case quick_action_command_name
      when 'label'
        add_only_label?(label)
      when 'unlabel'
        false # No labels only for unlabelling yet
      end
    end

    def add_only_label?(label)
      return true if EXTRA_ALLOWED_LABELS_ADD_ONLY.include?(label)
      return false if existing_scoped_label?(label)

      label.match?(EXTRA_ALLOWED_LABELS_REGEX_ADD_ONLY)
    end

    def existing_scoped_label?(label)
      label_scope = label[/\A[^:]+::/]

      return false unless label_scope

      event.label_names.any? { |label_name| label_name.start_with?(label_scope) }
    end

    def labels
      command.args(event)
    end

    def invalid_labels
      labels - labels_to_apply
    end

    def labels_to_apply
      @labels_to_apply ||=
        labels.select do |label|
          label.match?(ALLOWED_LABELS_REGEX) ||
            label.match?(CATEGORY_LABELS_REGEX) ||
            extra_allowed_label?(label)
        end
    end

    def command_labels
      labels_to_apply.map { |label| %(~"#{label}") }.join(' ')
    end

    def post_label_command
      comment = <<~MARKDOWN.chomp
        #{feedback_message}

        /#{quick_action_command_name} #{command_labels}
      MARKDOWN

      append_discussion(comment, append_source_link: false)
    end
  end
end
