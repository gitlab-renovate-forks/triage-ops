# frozen_string_literal: true

require 'digest'

require_relative 'community_processor'

require_relative '../../triage/changed_file_list'
require_relative '../../job/add_comment_job'
require_relative '../../../lib/www_gitlab_com'
require_relative '../../../lib/merge_request_coach_helper'

module Triage
  # Allows any community contributor to ask for a review.
  # The automation will pick a random MR coach and ask them to review the MR.
  class CommandMrRequestReview < CommunityProcessor
    include RateLimit
    include MergeRequestCoachHelper

    REVIEWERS_REGEX = /@([^@[[:blank:]]]+)/
    FIVE_MINUTES = 60 * 5
    PROJECTS_WITH_EXTERNAL_REVIEW_PROCESSES = [
      # https://gitlab.com/gitlab-org/security-products/gemnasium-db
      # The Vulnerability Research team has a rotating
      # reviewer for this project who reviews all MRs.
      12006272,
      # Exclude select Quality projects that have their own workflow
      # https://gitlab.com/gitlab-org/gitlab-environment-toolkit
      # https://gitlab.com/gitlab-org/quality/performance
      14292404,
      11511606
    ].freeze

    react_to 'merge_request.note'
    define_command name: 'ready', aliases: %w[review request_review], args_regex: REVIEWERS_REGEX

    def documentation
      <<~TEXT
      This allows any community contributor to ask for a review.

      The command can be used as `@gitlab-bot ready`, and is aliased to `@gitlab-bot review` and `@gitlab-bot request_review`.

      The command results in the `~"#{Labels::WORKFLOW_READY_FOR_REVIEW_LABEL}"` label being added to the merge request,
      and a review requested from a random available merge request coach.

      If you know a relevant reviewer(s) (for example, someone that was involved in a related issue),
      you can also assign them directly with `@gitlab-bot ready @user1 @user2`.
      TEXT
    end

    def applicable?
      valid_command?
    end

    def process
      comment = <<~MARKDOWN.chomp.strip
        #{review_request_comment if project_with_standard_review_process?}
        /ready
        /label ~"#{Labels::WORKFLOW_READY_FOR_REVIEW_LABEL}"
      MARKDOWN

      if diff_ready?
        append_discussion(comment, append_source_link: project_with_standard_review_process?)
      else
        append_discussion(no_changes_message, append_source_link: true)

        AddCommentJob.perform_in(FIVE_MINUTES, event, comment)
      end
    end

    def no_changes_message
      <<~TEXT
      @#{event.event_actor_username} this merge request currently contains no changes.
      If you have made changes, the diff may not yet be ready.

      We will wait 5 minutes before assigning a reviewer.

      If no reviewer is assigned, please use the command: `@gitlab-bot help`.
      TEXT
    end

    private

    def valid_reviewers
      @valid_reviewers ||= command.args(event)
    end

    def reviewer_mentions
      valid_reviewers.map { |reviewer| "@#{reviewer}" }.join(' ')
    end

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("request_review-commands-sent-#{event.event_actor_id}-#{event.noteable_path}-#{diff_ready?}")
    end

    def rate_limit_count
      event.by_team_member? ? 100 : 1
    end

    def rate_limit_period
      3600 # 1 hour
    end

    def diff_ready?
      Triage::ChangedFileList.new(project_id, merge_request_iid).merge_request_changes.any?
    end

    def project_id
      event.project_id
    end

    def merge_request_iid
      event.iid
    end

    def project_with_standard_review_process?
      !PROJECTS_WITH_EXTERNAL_REVIEW_PROCESSES.include?(event.project_id) &&
        !distribution_project?
    end

    # The Distribution team has their own review workflow so we don't assign until we're sure they're ok with this new workflow
    # See https://handbook.gitlab.com/handbook/engineering/infrastructure/core-platform/systems/distribution/merge_requests/#workflow
    def distribution_project?
      WwwGitLabCom.distribution_projects.include?(event.project_id)
    end

    def review_request_comment
      <<~MARKDOWN.strip
        #{intro_sentence}

        - Do you have capacity and domain expertise to review this? If not, find one or more [reviewers](https://gitlab-org.gitlab.io/gitlab-roulette/) and assign to them.
        - If you've reviewed it, add the ~"#{Labels::WORKFLOW_IN_DEV_LABEL}" label if these changes need more work before the next review.
        - Please ensure the group's Product Manager has validated the linked issue.

        /unlabel ~"#{Labels::AUTOMATION_AUTHOR_REMINDED_LABEL}"
      MARKDOWN
    end

    def intro_sentence
      if valid_reviewers.any?
        <<~MARKDOWN.strip
          #{reviewer_mentions}, this ~"#{Labels::COMMUNITY_CONTRIBUTION_LABEL}" is ready for review.
          /request_review #{reviewer_mentions}
        MARKDOWN
      elsif current_reviewers.any?
        <<~MARKDOWN.strip
          #{current_reviewers.join(' ')}, this ~"#{Labels::COMMUNITY_CONTRIBUTION_LABEL}" is ready for review.
          /request_review #{current_reviewers.join(' ')}
        MARKDOWN
      else
        <<~MARKDOWN.strip
          #{coach}, this ~"#{Labels::COMMUNITY_CONTRIBUTION_LABEL}" is ready for review.
          /request_review #{coach}
        MARKDOWN
      end
    end

    def current_reviewer_usernames
      @current_reviewer_usernames ||= merge_request.reviewers.map(&:username)
    end

    def current_reviewers
      @current_reviewers ||= current_reviewer_usernames.map { |reviewer| "@#{reviewer}" }
    end

    def merge_request
      @merge_request ||= Triage.api_client.merge_request(event.project_id, event.iid)
    end
  end
end
