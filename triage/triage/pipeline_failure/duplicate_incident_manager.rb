# frozen_string_literal: true

require_relative 'path_helper'
require_relative 'pipeline_incident_finder'
require_relative '../date_helper'
require_relative '../unique_comment'
require_relative '../event'
require_relative '../../../lib/constants/labels'

module Triage
  module PipelineFailure
    class DuplicateIncidentManager
      include PathHelper
      include DateHelper

      def initialize(config:, failed_job_names:)
        @config = config
        @failed_job_names = failed_job_names
      end

      def post_warning?
        config.escalate_on_stale? &&
          has_duplicate? &&
          unique_escalation_warning_comment.no_previous_comment?
      end

      def has_duplicate?
        !!duplicate_source_incident
      end

      def duplicate_source_incident
        return @duplicate_source_incident if defined?(@duplicate_source_incident)

        @duplicate_source_incident = find_duplicate_source_incident
      end

      def warning_comment_body
        comment_body = ['🚨 Multiple duplicate incidents detected!']

        escalation_comment = if today_is_weekend?
                               <<~MARKDOWN.chomp.prepend("\n\n")
            Automated escalation is not available on weekends and holidays.
            If immediate attention is required, please manually escalate to `#dev-escalaton` following [this handbook page](https://handbook.gitlab.com/handbook/engineering/workflow/#escalation-on-weekends-and-holidays).
            /label ~#{Labels::MASTER_BROKEN_INCIDENT_ESCALATION_LABELS[:needed]}
                               MARKDOWN
                             else
                               <<~MARKDOWN.chomp.prepend("\n\n")
            This incident will be escalated to `#dev-escalaton` in 4 hours of inactivity.
            /label ~#{Labels::MASTER_BROKEN_INCIDENT_ESCALATION_LABELS[:needed]}
                               MARKDOWN
                             end

        comment_body << escalation_comment unless skip_escalation?

        unique_escalation_warning_comment.wrap(comment_body.compact.join).strip
      end

      def canonical_incident_triggered_by_same_commit?(event)
        canonical_source_incident = previous_incidents.find do |previous_incident|
          previous_incident.title.include?('`gitlab-org/gitlab`') && previous_incident.description.include?(event.sha)
        end

        return false unless canonical_source_incident.present?

        @duplicate_source_incident = canonical_source_incident

        true
      end

      private

      attr_reader :config, :failed_job_names

      def issues_linked_to_source_incident
        @issues_linked_to_source_incident ||=
          Triage.api_client.issue_links(config.incident_project_id, duplicate_source_incident.iid)
      rescue Gitlab::Error::ResponseError
        []
      end

      def closed_as_duplicate_of_source_incident?(linked_issue)
        closed_as_duplicate_of_path = linked_issue._links['closed_as_duplicate_of']

        return false unless closed_as_duplicate_of_path

        url_pattern = incident_url_pattern(config, duplicate_source_incident)
        closed_as_duplicate_of_path.match?(url_pattern)
      end

      def unique_escalation_warning_comment
        @unique_escalation_warning_comment ||= Triage::UniqueComment.new(
          self.class.name,
          noteable_object_kind: 'issue',
          noteable_resource_iid: duplicate_source_incident.iid,
          noteable_project_id: config.incident_project_id
        )
      end

      def skip_escalation?
        duplicate_source_incident.labels.any? { |label| label.start_with?('escalation::') }
      end

      def find_duplicate_source_incident
        return unless previous_duplicate_incident

        # if the previous incident was closed as a duplicate of another, we want to return the original source
        closed_as_duplicate_of_link = previous_duplicate_incident._links['closed_as_duplicate_of']
        return previous_duplicate_incident unless closed_as_duplicate_of_link

        # closed_as_duplicate_of_link is an API URL, so we need to fetch the full payload
        Triage.api_client.get(closed_as_duplicate_of_link.gsub(%r{\Ahttps://.+/api/v4}, ''))
      end

      def previous_duplicate_incident
        @previous_duplicate_incident ||= previous_incidents.find do |previous_incident|
          failed_job_names_match_incident?(previous_incident)
        end
      end

      def previous_incidents
        @previous_incidents ||=
          Triage::PipelineFailure::PipelineIncidentFinder.new(
            incident_project_id: config.incident_project_id
          ).incidents
      end

      def failed_job_names_match_incident?(previous_incident)
        return incident_reported_empty_failed_jobs?(previous_incident) if failed_job_names.empty?

        failed_job_names.any? do |failed_job_name|
          # job names appear in a list of [job name](job url) markdowns in incident description
          # searching for "[job name]" instead of just "job name" ensure it's not a false match
          previous_incident.description.include?("[#{failed_job_name}]")
        end
      end

      def incident_reported_empty_failed_jobs?(incident)
        incident.description.include?('Failed jobs (0)')
      end
    end
  end
end
