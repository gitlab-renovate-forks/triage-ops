# frozen_string_literal: true
require 'active_support/inflector' # to use .demodulize

module Triage
  class UniqueComment
    NOTES_PER_PAGE = 100

    def initialize(
      class_name, event = nil, unique_comment_identifier = nil,
      from: nil,
      noteable_object_kind: nil, noteable_resource_iid: nil, noteable_project_id: nil
    )
      @class_name                = class_name
      @unique_comment_identifier = unique_comment_identifier
      @from                      = from
      @object_kind               = noteable_object_kind  || event&.object_kind
      @resource_iid              = noteable_resource_iid || event&.iid
      @project_id                = noteable_project_id || event&.project_id
    end

    def no_previous_comment?
      !previous_comment?
    end

    def previous_comment
      resource_notes.find { |note| matching_comment?(note) }
    end

    def previous_comment?
      !!previous_comment
    end

    def previous_discussion
      @previous_discussion ||= resource_discussions.find do |discussion|
        discussion['notes'].find do |note|
          matching_comment?(note)
        end
      end
    end

    def previous_discussion_comment
      return unless previous_discussion

      @previous_discussion_comment ||= previous_discussion['notes'].find do |note|
        matching_comment?(note)
      end
    end

    def wrap(message)
      "#{hidden_comment}\n#{message}"
    end

    def delete_previous_comment
      return unless previous_comment?

      path = "/projects/#{project_id}/#{normalized_object_kind}s/#{resource_iid}/notes/#{previous_comment.id}"
      result = Triage.api_client.delete(path)

      result.to_h == {} ? "#{path} deleted!" : result.to_h
    rescue StandardError => e
      "Failed to delete #{path}: #{e.message}"
    end

    private

    attr_reader :class_name, :unique_comment_identifier, :from, :object_kind, :resource_iid, :project_id

    def hidden_comment
      hidden_comment = unique_comment_identifier || class_name.demodulize
      "<!-- triage-serverless #{hidden_comment} -->"
    end

    def matching_comment?(note)
      # note is a Gitlab::ObjectifiedHash which is not a hash
      (from.nil? || from == note.to_h.dig('author', 'username')) &&
        note.body.include?(hidden_comment)
    end

    def resource_notes
      path = "/projects/#{project_id}/#{normalized_object_kind}s/#{resource_iid}/notes"

      Triage.api_client.get(path, query: { per_page: NOTES_PER_PAGE }).auto_paginate
    end

    def resource_discussions
      path = "/projects/#{project_id}/#{normalized_object_kind}s/#{resource_iid}/discussions"

      # merge_request_discussions do not support passing per_page
      Triage.api_client.get(path, query: { per_page: NOTES_PER_PAGE }).auto_paginate
    end

    def normalized_object_kind
      object_kind == 'incident' ? 'issue' : object_kind
    end
  end
end
