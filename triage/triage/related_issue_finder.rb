# frozen_string_literal: true

require_relative '../triage'

module Triage
  # Allow to find a related issue from an MR description.
  class RelatedIssueFinder
    TERMS = [
      'related to',
      'relates to',
      'relate to',
      'contributes to',
      'contribute to',
      'closes',
      'close',
      'see'
    ].freeze
    DEFAULT_RELATED_ISSUE_REGEX = %r{^(?:#{TERMS.join('|')}) (?<prefix>https://gitlab.com/(?<project_path>.*?)(/-/)?issues/|#)(?<iid>\d+)}i

    Issue = Struct.new(:project_id, :prefix, :project_path, :iid, keyword_init: true) do
      def labels
        @labels ||= issue_from_api&.labels || []
      end

      private

      def issue_from_api
        Triage.api_client.issue(project_id, iid)
      rescue Gitlab::Error::NotFound
        nil
      end
    end

    def initialize(related_issue_regex: DEFAULT_RELATED_ISSUE_REGEX)
      @related_issue_regex = related_issue_regex
    end

    def find_issue_in(text)
      matches = matches_for(text)
      return unless matches

      Issue.new(matches)
    end

    def find_project_issue_in(project_id, text)
      matches = matches_for(text)
      return unless matches

      project_path_or_id = matches['project_path'] || project_id

      Issue.new(project_id: project_path_or_id, iid: matches['iid'])
    end

    private

    attr_reader :related_issue_regex

    def matches_for(text)
      matches = related_issue_regex.match(text)
      return unless matches

      matches.named_captures
    end
  end
end
