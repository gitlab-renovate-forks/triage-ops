.common_rules: &common_rules
  limits:
    most_recent: 30

.item: &item
  item: |
    - [ ] #{full_resource_reference} {{title}} {{labels}}

.number_list_item: &number_list_item
  item: |
    1. #{full_resource_reference} {{title}} {{labels}}

.conditions_unscheduled_customer: &conditions_unscheduled_customer
  state: opened
  milestone: none

.conditions_unscheduled_non_customer: &conditions_unscheduled_non_customer
  state: opened
  forbidden_labels:
    - customer
  milestone: none

.conditions_unscheduled_ux_debt: &conditions_unscheduled_ux_debt
  state: opened
  milestone: none

.conditions_unscheduled_ui_polish: &conditions_unscheduled_ui_polish
  state: opened
  milestone: none

.conditions_open_infradev_issues: &conditions_open_infradev_issues
  state: opened

.conditions_this_weeks_open_infradev_issues: &conditions_this_weeks_open_infradev_issues
  state: opened
  date:
    attribute: created_at
    condition: newer_than
    interval_type: days
    interval: 7

.conditions_overdue_open_infradev_issues: &conditions_overdue_open_infradev_issues
  state: opened
  ruby: overdue?

.conditions_heatmap: &conditions_heatmap
  state: opened

.conditions_unscheduled_remaining_customer_bugs: &conditions_unscheduled_remaining_customer_bugs
  state: opened
  forbidden_labels:
    - frontend
  milestone: none

.conditions_unscheduled_remaining_bugs: &conditions_unscheduled_remaining_bugs
  state: opened
  forbidden_labels:
    - frontend
    - customer
  milestone: none

.conditions_open_quarantined_flaky_specs: &conditions_open_quarantined_flaky_specs
  state: opened
  forbidden_labels:
    - QA

.summarize_unscheduled_customer_features: &summarize_unscheduled_customer_features
  summarize:
    <<: *item
    title: unscheduled customer feature proposals
    summary: |
      #{ feature_proposal_intro + unscheduled_customer_feature_summary }

.summarize_unscheduled_features: &summarize_unscheduled_features
  summarize:
    <<: *item
    title: unscheduled feature proposals
    summary: |
      #{ unscheduled_feature_summary }

.summarize_unscheduled_ux_debt: &summarize_unscheduled_ux_debt
  summarize:
    <<: *item
    title: unscheduled ux debt
    summary: |
      #{ unscheduled_ux_debt_summary }

.summarize_unscheduled_ui_polish: &summarize_unscheduled_ui_polish
  summarize:
    <<: *item
    title: unscheduled ui polish
    summary: |
      #{ unscheduled_ui_polish_summary }

.summarize_infradev_issues: &summarize_infradev_issues
  summarize:
    <<: *item
    title: New Infradev Issues
    summary: |
      #{ infradev_section_intro + infradev_latest_issues_summary }

.summarize_overdue_infradev_issues: &summarize_overdue_infradev_issues
  summarize:
    <<: *item
    title: Overdue Infradev Issues
    summary: |
      #{ infradev_overdue_issues_summary }

.summarize_all_infradev_heatmap: &summarize_all_infradev_heatmap
  summarize:
    <<: *item
    title: Heatmap for all Infradev Issues
    summary: |
      #{ infradev_all_issues_heatmap }

.summarize_security_heatmap: &summarize_security_heatmap
  summarize:
    title: Heatmap for security issues
    summary: |
      #{ security_section_intro + security_heatmap }

.summarize_availability_heatmap: &summarize_availability_heatmap
  summarize:
    title: Heatmap for availability issues
    summary: |
      #{ availability_section_intro + availability_heatmap }

.summarize_bug_heatmap: &summarize_bug_heatmap
  summarize:
    title: Heatmap for bugs
    summary: |
      #{ bug_section_intro + bug_heatmap }

.summarize_ux_debt_heatmap: &summarize_ux_debt_heatmap
  summarize:
    title: Heatmap for UX debt issues
    summary: |
      #{ ux_debt_section_intro + ux_debt_heatmap }

.summarize_ui_polish_heatmap: &summarize_ui_polish_heatmap
  summarize:
    title: Heatmap for UI polish issues
    summary: |
      #{ ui_polish_section_intro + ui_polish_heatmap }

.summarize_unscheduled_frontend_customer_bugs: &summarize_unscheduled_frontend_customer_bugs
  summarize:
    <<: *item
    title: Frontend unscheduled Customer Bugs
    summary: |
      #{ frontend_unscheduled_customer_bugs_summary }

.summarize_unscheduled_frontend_bugs: &summarize_unscheduled_frontend_bugs
  summarize:
    <<: *item
    title: Frontend unscheduled Bugs
    summary: |
      #{ frontend_unscheduled_bugs_summary }

.summarize_unscheduled_remaining_customer_bugs: &summarize_unscheduled_remaining_customer_bugs
  summarize:
    <<: *item
    title: Backend/remaining customer bugs
    summary: |
      #{ remaining_unscheduled_customer_bugs_summary }

.summarize_unscheduled_remaining_bugs: &summarize_unscheduled_remaining_bugs
  summarize:
    <<: *item
    title: Backend remaining bugs
    summary: |
      #{ remaining_unscheduled_bugs_summary }

.summarize_missed_slo_heatmap: &summarize_missed_slo_heatmap
  summarize:
    title: Heatmap for all bugs past SLO
    summary: |
      #{ missed_slo_heatmap }

.summarize_vintage_heatmap: &summarize_vintage_heatmap
  summarize:
    title: Heatmap for all vintage bugs
    summary: |
      #{ vintage_heatmap }

.summarize_customer_bugs_heatmap: &summarize_customer_bugs_heatmap
  summarize:
    title: Heatmap for all customer bugs
    summary: |
      #{ customer_bugs_heatmap }

.summarize_open_quarantined_flaky_specs: &summarize_open_quarantined_flaky_specs
  summarize:
    <<: *item
    title: Open quarantined flaky specs Issues
    summary: |
      #{ quarantined_flaky_specs_summary }

resource_rules:
  issues:
    summaries:
      - name: Collate "group::runner" issues requiring attention
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{ report_title("group::runner") }
            summary: |
              #{ report_summary(group_key: "runner") }

              ---

              Job URL: #{ENV['CI_JOB_URL']}

              This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/#{ENV['TRIAGE_POLICY_FILE']})
        rules:
          - name: ~"group::runner" Security Heatmap (all)
            conditions:
              <<: *conditions_heatmap
              labels:
                - group::runner
                - security
            actions:
              <<: *summarize_security_heatmap
          - name: ~"group::runner" Availability Heatmap (all)
            conditions:
              <<: *conditions_heatmap
              labels:
                - group::runner
                - availability
            actions:
              <<: *summarize_availability_heatmap
          - name: ~"group::runner" New Infradev issues this week
            conditions:
              <<: *conditions_this_weeks_open_infradev_issues
              labels:
                - group::runner
                - infradev
            actions:
              <<: *summarize_infradev_issues
          - name: ~"group::runner" Infradev issues past SLO
            conditions:
              <<: *conditions_overdue_open_infradev_issues
              labels:
                - group::runner
                - infradev
            actions:
              <<: *summarize_overdue_infradev_issues
          - name: ~"group::runner" Heatmap for all Infradev issues
            conditions:
              <<: *conditions_open_infradev_issues
              labels:
                - group::runner
                - infradev
            actions:
              <<: *summarize_all_infradev_heatmap
          - name: ~"group::runner" unscheduled customer proposals
            <<: *common_rules
            conditions:
              <<: *conditions_unscheduled_customer
              labels:
                - group::runner
                - type::feature
                - customer
            actions:
              <<: *summarize_unscheduled_customer_features
          - name: ~"group::runner" unscheduled proposals
            <<: *common_rules
            conditions:
              <<: *conditions_unscheduled_non_customer
              labels:
                - group::runner
                - type::feature
            actions:
              <<: *summarize_unscheduled_features
          - name: ~"group::runner" ux debt
            <<: *common_rules
            conditions:
              <<: *conditions_unscheduled_ux_debt
              labels:
                - group::runner
                - UX debt
            actions:
              <<: *summarize_unscheduled_ux_debt
          - name: ~"group::runner" Bug Heatmap (all)
            conditions:
              <<: *conditions_heatmap
              labels:
                - group::runner
                - type::bug
            actions:
              <<: *summarize_bug_heatmap
          - name: Customer bugs Heatmap
            conditions:
              <<: *conditions_heatmap
              labels:
                - group::runner
                - customer
                - type::bug
            actions:
              <<: *summarize_customer_bugs_heatmap
          - name: ~"group::runner" frontend customer bugs
            <<: *common_rules
            conditions:
              <<: *conditions_unscheduled_customer
              labels:
                - group::runner
                - type::bug
                - frontend
                - customer
            actions:
              <<: *summarize_unscheduled_frontend_customer_bugs
          - name: ~"group::runner" frontend bugs
            <<: *common_rules
            conditions:
              <<: *conditions_unscheduled_non_customer
              labels:
                - group::runner
                - type::bug
                - frontend
            actions:
              <<: *summarize_unscheduled_frontend_bugs
          - name: ~"group::runner" customer remaining bugs
            <<: *common_rules
            conditions:
              <<: *conditions_unscheduled_remaining_customer_bugs
              labels:
                - group::runner
                - type::bug
                - customer
            actions:
              <<: *summarize_unscheduled_remaining_customer_bugs
          - name: ~"group::runner" remaining bugs
            <<: *common_rules
            conditions:
              <<: *conditions_unscheduled_remaining_bugs
              labels:
                - group::runner
                - type::bug
            actions:
              <<: *summarize_unscheduled_remaining_bugs
          - name: Missed SLO Heatmap
            conditions:
              <<: *conditions_heatmap
              labels:
                - group::runner
                - SLO::Missed
            actions:
              <<: *summarize_missed_slo_heatmap
          - name: Vintage bugs Heatmap
            conditions:
              <<: *conditions_heatmap
              labels:
                - group::runner
                - type::bug
                - vintage
            actions:
              <<: *summarize_vintage_heatmap
          - name: ~Open quarantined flaky specs
            <<: *common_rules
            conditions:
              <<: *conditions_open_quarantined_flaky_specs
              labels:
                - group::runner
                - failure::flaky-test
                - quarantined test
            actions:
              <<: *summarize_open_quarantined_flaky_specs
