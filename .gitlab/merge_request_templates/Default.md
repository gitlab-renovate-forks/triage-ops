## What does this MR do and why?

_Describe in detail what your merge request does and why._

<!--
Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

%{first_multiline_commit}

## Expected impact & dry-runs

_These are strongly recommended to assist reviewers and reduce the time to merge your change._

See https://gitlab.com/gitlab-org/quality/triage-ops/-/tree/master/doc/scheduled#testing-policies-with-a-dry-run on how to perform dry-runs for new policies.

See https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/doc/reactive/best_practices.md#use-the-sandbox-to-test-new-processors on how to make sure a new processor can be tested.

## Action items

* [ ] If adding environment variables for [reactive processors](https://gitlab.com/gitlab-community/quality/triage-ops/-/blob/master/doc/reactive/index.md), update [`config/triage-web.yaml`](https://gitlab.com/gitlab-community/quality/triage-ops/-/blob/master/config/triage-web.yaml) and [`.gitlab/ci/triage-web.yml`](https://gitlab.com/gitlab-community/quality/triage-ops/-/blob/master/.gitlab/ci/triage-web.yml)
* [ ] (If applicable) Add documentation to the handbook pages for [Triage Operations](https://gitlab.com/gitlab-com/content-sites/handbook/blob/main/content/handbook/engineering/infrastructure/engineering-productivity/triage-operations/_index.md) =>
* (If applicable) Identify the affected groups and how to communicate to them:
  * [ ] /cc @`person_or_group` =>
  * [ ] Relevant Slack channels =>
  * [ ] Engineering week-in-review

/label ~"maintenance::workflow" ~"type::maintenance"
/assign me

<!-- template sourced from https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/.gitlab/merge_request_templates/Default.md -->
