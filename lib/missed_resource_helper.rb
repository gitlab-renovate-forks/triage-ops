# frozen_string_literal: true

require_relative 'versioned_milestone'
require_relative 'www_gitlab_com'
require_relative 'milestone_helper'
require 'date'

module MissedResourceHelper
  def missed_resource?(milestone, current_date = today)
    return false unless milestone&.due_date

    current_date > milestone.due_date
  end

  def add_missed_labels(milestone_title, labels)
    result = [
      %(/label ~"missed:#{milestone_title}")
    ]

    result << '/label ~"missed-deliverable"' if labels.index('Deliverable')

    result.join("\n")
  end

  def remove_deliverable_labels
    %(/unlabel ~"Deliverable" ~"goal::stretch" ~"goal::planning" ~"goal::development" ~"goal::complete")
  end

  def move_to_current_milestone
    current_milestone = VersionedMilestone.new(self).current

    %(/milestone %"#{current_milestone.title}") unless current_milestone.nil?
  end

  def move_to_next_milestone
    next_milestone_title = VersionedMilestone.new(self).next.title

    %(/milestone %"#{next_milestone_title}")
  end

  private

  def today
    if fake_today = ENV['TRIAGE_FAKE_TODAY_FOR_MISSED_RESOURCES']
      Date.parse(fake_today)
    else
      Date.today
    end
  end
end
