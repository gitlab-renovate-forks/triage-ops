# frozen_string_literal: true

# TODO: check if all requires are needed
require_relative 'constants/labels'
require_relative '../triage/triage'
require_relative 'shared/resource_notes_helper'

module GrowthTimeInDevHelper
  include ResourceNotesHelper

  IN_DEV_LABEL = Labels::WORKFLOW_IN_DEV_LABEL
  ONE_WEEK_IN_SECONDS = 7 * 24 * 60 * 60
  TWO_WEEK_IN_SECONDS = 2 * ONE_WEEK_IN_SECONDS
  THREE_WEEK_IN_SECONDS = 3 * ONE_WEEK_IN_SECONDS
  FOUR_WEEK_IN_SECONDS = 4 * ONE_WEEK_IN_SECONDS

  WEEKS = {
    one: {
      more: ONE_WEEK_IN_SECONDS,
      less: TWO_WEEK_IN_SECONDS
    },
    two: {
      more: TWO_WEEK_IN_SECONDS,
      less: THREE_WEEK_IN_SECONDS
    },
    three: {
      more: THREE_WEEK_IN_SECONDS,
      less: FOUR_WEEK_IN_SECONDS
    },
    four: {
      more: FOUR_WEEK_IN_SECONDS,
      less: Float::INFINITY
    }
  }.freeze

  def over_one_week_in_dev?
    over_weeks_in_dev?(project_id, resource_iid, :one)
  end

  def over_two_weeks_in_dev?
    over_weeks_in_dev?(project_id, resource_iid, :two)
  end

  def over_three_weeks_in_dev?
    over_weeks_in_dev?(project_id, resource_iid, :three)
  end

  def over_four_weeks_in_dev?
    over_weeks_in_dev?(project_id, resource_iid, :four)
  end

  private

  def calculate_time_in_dev(label_events)
    in_dev_timestamps = label_events
      .select { |event| event.label.name == IN_DEV_LABEL }
      .map { |event| Time.parse(event.created_at) }

    in_dev_timestamps << Time.now.utc if in_dev_timestamps.size.odd?

    time_in_dev = 0
    in_dev_timestamps.each_slice(2) { |added, removed| time_in_dev += removed - added }

    time_in_dev
  rescue ArgumentError, TypeError
    0
  end

  def over_weeks_in_dev?(project_id, iid, weeks)
    label_events = Triage.api_client.issue_label_events(project_id, iid).auto_paginate

    time_in_dev = calculate_time_in_dev(label_events)
    time_in_dev >= WEEKS[weeks][:more] && time_in_dev < WEEKS[weeks][:less]
  end
end
