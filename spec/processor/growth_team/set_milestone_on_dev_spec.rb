# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/growth_team/set_milestone_on_dev'
require_relative '../../../triage/triage/event'
require_relative '../../../lib/constants/labels'

RSpec.describe Triage::SetMilestoneOnDev do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        from_gitlab_org?: true,
        added_label_names: [Labels::WORKFLOW_IN_DEV_LABEL],
        label_names: Labels::GROWTH_TEAM_LABELS
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.update', 'issue.open']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when issue does not belong to growth team' do
      before do
        allow(event).to receive(:added_label_names).and_return(Labels::SECURITY_INSIGHTS_GROUP_LABEL)
        allow(event).to receive(:label_names).and_return(Labels::SECURITY_INSIGHTS_GROUP_LABEL)
      end

      include_examples 'event is not applicable'
    end

    context "when 'workflow::in dev' label is not added" do
      before do
        allow(event).to receive(:added_label_names).and_return(Labels::GROWTH_TEAM_LABELS)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:gitlab_org_group_id) { described_class::GITLAB_ORG_GROUP_ID }

    let(:milestones) do
      [
        { title: '16.3', expired: true },
        { title: 'Git 2.48', expired: false },
        { title: '16.4', expired: false },
        { title: '16.5', expired: false },
        { title: '16.6', expired: false }
      ]
    end

    before do
      stub_api_request(path: "/groups/#{gitlab_org_group_id}/milestones?state=active", response_body: milestones)
    end

    it 'posts a comment with quick action to assign the correct current milestone' do
      body = "/milestone %16.4"

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
