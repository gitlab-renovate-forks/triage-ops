# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/handbook_maintainer_ops_notifier'
require_relative '../../triage/triage/event'

RSpec.describe Triage::HandbookMaintainerOpsNotifier do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        iid: merge_request_iid,
        from_gitlab_org?: true
      }
    end
  end

  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => ".gitlab-ci.yml",
          "new_path" => "something/new.md"
        }
      ]
    }
  end

  let(:from_handbook) { true }

  include_context 'with merge request notes'

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
    allow(event).to receive(:from_handbook?).and_return(from_handbook)
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.update', 'merge_request.open']

  describe '#applicable?' do
    include_examples 'event is applicable'

    context 'when event project is not the public or private handbook' do
      let(:from_handbook) { false }

      include_examples 'event is not applicable'
    end

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { body: 'review comment 1' },
          { body: comment_mark }
        ]
      end

      include_examples 'event is not applicable'
    end

    context 'when relevant files are not changed' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "something/old.md",
              "new_path" => "something/new.md"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a review request comment' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          When you are ready for this MR to be reviewed, please assign a relevant maintainer, or tag `@gitlab-com/content-sites/handbook-tools` as it modifies `.gitlab-ci.yml`, `assets/`, `config/`, `layouts`, `scripts/`, `issue_templates/`, or `merge_request_templates/`.
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
