# frozen_string_literal: true

require 'spec_helper'
require 'gitlab/triage/resource/context'
require_relative '../../plugins/issue_triage_helper'

RSpec.describe IssueTriageHelper do
  let(:concrete_class) do
    Class.new do
      include IssueTriageHelper

      def resource
        {
          title: 'Title',
          description: 'Description',
          labels: ['bug'],
          project_id: 123,
          author: { username: 'author' }
        }
      end
    end
  end

  let(:ai_triage_helper) { instance_double(AiTriageHelper) }

  before do
    allow(AiTriageHelper).to receive(:new).and_return(ai_triage_helper)
    allow(ai_triage_helper).to receive(:execute).and_return('result')
  end

  describe '.issue_triage_ai_comment' do
    it 'returns expected message' do
      expect(concrete_class.new.issue_triage_ai_comment).to eq('result')
    end

    it 'calls ai_triage_helper with expected arguments' do
      concrete_class.new.issue_triage_ai_comment

      expect(ai_triage_helper).to have_received(:execute).with(
        'Title',
        'Description',
        ['bug'],
        'author'
      )
    end
  end
end
