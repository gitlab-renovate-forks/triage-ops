# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/notified_channels'
require_relative '../../../triage/triage/pipeline_failure/config/master_branch'

RSpec.describe Triage::PipelineFailure::NotifiedChannels do
  let(:event)  { instance_double(Triage::PipelineEvent, id: 123, project_id: Triage::Event::GITLAB_PROJECT_ID, instance: :com) }
  let(:config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }

  subject(:notified_channels) { described_class.new(config, label) }

  describe '#to_a' do
    context 'with test-level:e2e label' do
      let(:label) { 'test-level:e2e' }

      it 'returns channel names in an array' do
        expect(subject.to_a).to eq(%w[master-broken e2e-run-master])
      end
    end

    context 'with group1 label' do
      let(:label) { 'group::group1' }

      it 'returns channel names in an array' do
        expect(subject.to_a).to eq(%w[master-broken group1])
      end
    end
  end

  describe '#to_s' do
    context 'with test-level:e2e label' do
      let(:label) { 'test-level:e2e' }

      it 'returns channel names in an array' do
        expect(subject.to_s).to eq('`#master-broken`, `#e2e-run-master`')
      end
    end

    context 'with group1 label' do
      let(:label) { 'group::group1' }

      it 'returns channel names in an array' do
        expect(subject.to_s).to eq('`#master-broken`, `#group1`')
      end
    end
  end
end
