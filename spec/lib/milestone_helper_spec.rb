# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/versioned_milestone'
require_relative '../../lib/milestone_helper'

RSpec.describe MilestoneHelper do
  let(:resource_klass) do
    Class.new do
      include MilestoneHelper
    end
  end

  let(:versioned_milestone) { VersionedMilestone.new(resource) }
  let(:resource) { resource_klass.new }

  describe '#set_milestone_for_merge_request' do
    let(:merge_request) { { merged_at: "2023-11-07T11:16:17.520Z" } }
    let(:milestone) { instance_double(Gitlab::Triage::Resource::Milestone, title: '16.6') }

    before do
      allow(VersionedMilestone).to receive(:new).with(resource).and_return(versioned_milestone)
      allow(versioned_milestone).to receive(:find_milestone_for_date).and_return(milestone)
    end

    it 'sets the milestone for the merge request' do
      expect(resource.set_milestone_for_merge_request(merge_request)).to eq('/milestone %"16.6"')
    end

    context 'when no milestone exist for the merge request' do
      let(:milestone) { nil }

      it 'returns early' do
        expect(resource.set_milestone_for_merge_request(merge_request)).to be_nil
      end
    end
  end

  describe '#next_milestone' do
    let(:next_milestone) { instance_double(Gitlab::Triage::Resource::Milestone, title: '16.7') }

    before do
      allow(VersionedMilestone).to receive(:new).with(resource).and_return(versioned_milestone)
      allow(versioned_milestone).to receive(:next).and_return(next_milestone)
    end

    it 'returns the next milestone object' do
      expect(resource.next_milestone.title).to eq('16.7')
    end
  end
end
