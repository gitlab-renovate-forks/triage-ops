# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/hierarchy/people_set'

RSpec.describe Hierarchy::PeopleSet do
  let(:concrete_class) do
    Class.new(described_class) do
      def self.raw_data
        {
          'team_key' => {
            'name' => 'Team Name',
            'label' => 'concrete_label',
            'extra_labels' => ['extra_label'],
            'slack_channel' => 'slack_channel',
            'product_managers' => ['product_manager'],
            'engineering_managers' => ['engineering_manager'],
            'backend_engineers' => ['backend_engineer'],
            'frontend_engineers' => ['frontend_engineer']
          },
          'another_team_key' => {}
        }
      end
    end
  end

  subject(:team) { concrete_class.new('team_key') }

  describe '.raw_data' do
    it 'raises an exception' do
      expect { described_class.raw_data }.to raise_error('Not implemented')
    end
  end

  describe '.all' do
    it 'returns an array of people set' do
      teams = concrete_class.all

      expect(teams).to be_a(Array)
      expect(teams.first).to be_a(concrete_class)
      expect(teams.first.key).to eq('team_key')
    end
  end

  describe '.exist?' do
    context 'when team key exists' do
      it 'returns true' do
        expect(concrete_class.exist?('team_key')).to be(true)
      end
    end

    context 'when team key does not exist' do
      it 'returns false' do
        expect(concrete_class.exist?('unknown_team')).to be(false)
      end
    end
  end

  describe '.all_labels' do
    it 'includes the label from the raw data' do
      expect(concrete_class.all_labels).to include('concrete_label')
    end
  end

  describe '.find_by_label' do
    context 'when a people set matches the label' do
      it 'returns a PeopleSet instance matching the label' do
        expect(concrete_class.find_by_label('concrete_label')).to be_a(concrete_class)
        expect(concrete_class.find_by_label('concrete_label').key).to eq('team_key')
      end
    end

    context 'when a person set matches the label case-insensitively' do
      it 'returns a PeopleSet instance matching the label' do
        expect(concrete_class.find_by_label('Concrete_Label')).to be_a(concrete_class)
        expect(concrete_class.find_by_label('Concrete_Label').key).to eq('team_key')
      end
    end

    context 'when no people set matches the label' do
      it 'returns nil' do
        expect(concrete_class.find_by_label('unknown_label')).to be_nil
      end
    end

    context 'when label is nil' do
      it 'returns nil' do
        expect(concrete_class.find_by_label(nil)).to be_nil
      end
    end
  end

  describe '.find_by_name' do
    context 'when a people set matches the name' do
      it 'returns a PeopleSet instance matching the name' do
        expect(concrete_class.find_by_name('Team Name')).to be_a(concrete_class)
        expect(concrete_class.find_by_name('Team Name').key).to eq('team_key')
      end
    end

    context 'when a person set matches the name case-insensitively' do
      it 'returns a PeopleSet instance matching the name' do
        expect(concrete_class.find_by_name('TEAM NAME')).to be_a(concrete_class)
        expect(concrete_class.find_by_name('TEAM NAME').key).to eq('team_key')
      end
    end

    context 'when no people set matches the name' do
      it 'returns nil' do
        expect(concrete_class.find_by_name('unknown name')).to be_nil
      end
    end

    context 'when name is nil' do
      it 'returns nil' do
        expect(concrete_class.find_by_name(nil)).to be_nil
      end
    end
  end

  describe '.initialize' do
    it 'finds the team by key' do
      team = concrete_class.new('team_key')

      expect(team).to be_a(concrete_class)
      expect(team.key).to eq('team_key')
    end
  end

  describe '#==' do
    context 'when team key is the same as other.key' do
      it 'returns true' do
        expect(team == concrete_class.new('team_key')).to be_truthy
      end
    end

    context 'when team key is different than the other.key' do
      it 'returns false' do
        expect(team == concrete_class.new('another_team_key')).to be_falsy
      end
    end

    context 'when other is nil' do
      it 'returns false' do
        expect(team == nil).to be_falsy # rubocop:disable Style/NilComparison
      end
    end
  end

  describe '#slug' do
    context 'when team key is already a slug' do
      it 'returns the team key as slug' do
        expect(team.slug).to eq('team_key')
      end
    end

    context 'when team key is not already a slug' do
      let(:concrete_class) do
        Class.new(described_class) do
          def self.raw_data
            {
              'a-team-key' => {}
            }
          end
        end
      end

      subject(:team) { concrete_class.new('a-team-key') }

      it 'returns a slugified team key' do
        expect(team.slug).to eq('a_team_key')
      end
    end
  end

  describe '#name' do
    it 'returns the team name' do
      expect(team.name).to eq('Team Name')
    end
  end

  describe '#label' do
    it 'returns the team label' do
      expect(team.label).to eq('concrete_label')
    end
  end

  describe '#labels' do
    it 'returns all labels' do
      expect(team.labels).to eq(%w[concrete_label extra_label])
    end
  end

  describe '#extra_labels' do
    it 'returns all extra_labelss' do
      expect(team.extra_labels).to eq(['extra_label'])
    end
  end

  describe '#slack_channel' do
    it 'returns the team slack_channel' do
      expect(team.slack_channel).to eq('slack_channel')
    end

    described_class::SLACK_CHANNELS_OVERRIDES.each do |group_channel, overridden_channel|
      context "when slack channel is overridden from '#{group_channel}' to '#{overridden_channel}'" do
        let(:team_data) { { 'team_key' => { 'slack_channel' => group_channel } } }
        let(:concrete_class_with_override) do
          Class.new(described_class) do
            def self.raw_data
              @team_data
            end

            def self.set_raw_data(data)
              @team_data = data
              self
            end
          end
        end

        it 'returns the overriden slack channel' do
          team = concrete_class_with_override.set_raw_data(team_data).new('team_key')
          expect(team.slack_channel).to eq(overridden_channel)
        end
      end
    end
  end

  describe '#assignees' do
    context 'when fields is not passed' do
      it 'returns all default assignees' do
        expect(team.assignees).to eq(%w[@engineering_manager @product_manager])
      end
    end

    context 'when fields are passed' do
      it 'returns matching assignees' do
        expect(team.assignees(%w[engineering_managers])).to eq(['@engineering_manager'])
      end
    end
  end

  describe '#mentions' do
    context 'when fields are not passed' do
      it 'returns all default mentions' do
        expect(team.mentions).to eq(%w[@backend_engineer @frontend_engineer])
      end
    end

    context 'when fields are passed' do
      it 'returns []' do
        expect(team.mentions(%w[@backend_engineer])).to eq([])
      end
    end
  end

  describe '#to_h' do
    it 'returns a hash' do
      expect(team.to_h.keys).to eq(%i[assignees labels mentions])
    end
  end
end
