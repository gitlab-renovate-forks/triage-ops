# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/broken_master_label_nudger_job'
require_relative '../../triage/triage/event'
require_relative '../../lib/constants/labels'

RSpec.describe Triage::BrokenMasterLabelNudgerJob do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      { project_id: project_id,
        event_actor_username: 'username',
        label_names: [] }
    end
  end

  let(:project_id)           { Triage::Event::MASTER_BROKEN_INCIDENT_PROJECT_ID }
  let(:label_names)          { ['master-broken::undetermined'] }
  let(:issue_notes_api_path) { "/projects/#{project_id}/issues/#{iid}/notes" }
  let(:issue_path)           { "/projects/#{project_id}/issues/#{iid}" }
  let(:issue_attr) do
    { 'iid' => iid, 'labels' => label_names }
  end

  subject { described_class.new }

  before do
    stub_api_request(path: issue_path, response_body: issue_attr)
  end

  describe '#perform' do
    context 'when incident does not require any label change' do
      let(:label_names) { ['master-broken::infrastructure'] }

      it 'does nothing' do
        expect_no_request { subject.perform(event) }
      end
    end

    context 'when missing root cause label' do
      it 'nudges for root cause label' do
        body = add_automation_suffix do
          <<~MARKDOWN.chomp
            <!-- triage-serverless BrokenMasterLabelNudgerJob -->
            :wave: @username, #{described_class::ROOT_CAUSE_LABEL_MISSING_MESSAGE} following the [Triage DRI Responsibilities handbook page](https://handbook.gitlab.com/handbook/engineering/workflow/#triage-dri-responsibilities).

            Ignore this comment if you think the incident already has all of the necessary labels to conclude your root cause analysis.
          MARKDOWN
        end

        expect_api_request(
          verb: :post,
          request_body: { body: body },
          path: issue_notes_api_path,
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end

    context 'when missing flaky test label' do
      let(:label_names) { ['master:broken', 'master-broken::flaky-test'] }

      it 'nudges for flaky test label' do
        body = add_automation_suffix do
          <<~MARKDOWN.chomp
            <!-- triage-serverless BrokenMasterLabelNudgerJob -->
            :wave: @username, please identify the flaky root cause with a `~flaky-test::` label following the [Triage DRI Responsibilities handbook page](https://handbook.gitlab.com/handbook/engineering/workflow/#triage-dri-responsibilities).

            Ignore this comment if you think the incident already has all of the necessary labels to conclude your root cause analysis.
          MARKDOWN
        end

        expect_api_request(
          verb: :post,
          request_body: { body: body },
          path: issue_notes_api_path,
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end
  end
end
