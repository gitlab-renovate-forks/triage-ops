# GitLab triage operations

- [Choose your strategy: Reactive or Scheduled](#choose-your-strategy-reactive-or-scheduled)
- [The bot](#the-bot)
- [Development](#development)
  - [Install gems](#install-gems)
  - [Activate lefthook locally](#activate-lefthook-locally)
  - [Run tests](#run-tests)
  - [Language Server](#language-server)


*This is a collection of prototype tools that help triage issues and merge requests.  Support for triage operations is proposed in epic [636](https://gitlab.com/groups/gitlab-org/-/epics/636).*

## Choose your strategy: Reactive or Scheduled

Triage operations for GitLab involves two strategies: reactive and scheduled operations. See dedicated documentation for them.

* [**Reactive operations**](doc/reactive/index.md)
* [**Scheduled operations**](doc/scheduled/index.md)

## The bot

You may have seen [@gitlab-bot](https://gitlab.com/gitlab-bot) commenting on GitLab
issues. The same bot is also used in the following projects:

* https://gitlab.com/gitlab-org/release-tools
* https://gitlab.com/gitlab-org/async-retrospectives
* https://gitlab.com/gitlab-com/gl-infra/triage-ops

We are not leveraging a bot framework, but both reactive hooks and scheduled
operations authenticate as [@gitlab-bot](https://gitlab.com/gitlab-bot), which credentials can be found in the shared 1Password vault.

For a code example, see how
[the reactive hook](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/command_mr_label.rb)
implements `@gitlab-bot label ...` command.


## Development

### Install gems

```
bundle install
```

### Activate lefthook locally

```shell
lefthook install
```

### Run tests

You can run the test suite with `bundle exec rspec`.

Guard is also supported (see the [`Guardfile`](Guardfile)). Install Guard with `gem install guard`, then start it with `guard`.

### Language Server

Optionally install the [Solargraph](https://solargraph.org) plugin for your editor.
