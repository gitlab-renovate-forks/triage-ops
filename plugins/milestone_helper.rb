# frozen_string_literal: true

require_relative '../lib/milestone_helper'

Gitlab::Triage::Resource::Context.include MilestoneHelper
